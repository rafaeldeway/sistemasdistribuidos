package exercicio2_1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class Principal {
	
	public static void main(String[] args) {
		
		InvertOutputStream invertOutputStream = new InvertOutputStream(System.out);
		
		String mensagem = "ABCD";
		byte[] bytes = new byte[mensagem.length()];
		bytes = mensagem.getBytes();
		try {
			invertOutputStream.write(bytes);
			invertOutputStream.flush();
            
            mensagem = "FGHI";
    		bytes = new byte[mensagem.length()];
    		bytes = mensagem.getBytes();

    		
            invertOutputStream.write(bytes, 2, 2);
			invertOutputStream.flush();
			invertOutputStream.close();			
	
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
