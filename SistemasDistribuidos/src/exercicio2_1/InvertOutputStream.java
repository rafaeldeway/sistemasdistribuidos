package exercicio2_1;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InvertOutputStream extends OutputStream{
	
	private OutputStream outputStream;

	@Override
	public void write(int b) throws IOException {
		outputStream.write(b);
	}

	public InvertOutputStream(OutputStream outputStream) {
		super();
		this.outputStream = outputStream;
	}

	@Override
	public void write(byte[] b) throws IOException {
		int tamanho = b.length;
		for (int i= tamanho-1; i>=0; i--){
			write(b[i]);
		}
		outputStream.flush();
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		byte[] bytes = new byte[len];
		for (int i=0; i<len; i++){
			bytes[i] = b[off];
			off++;
		}
		
		for (int i= bytes.length-1; i>=0; i--){
			write(bytes[i]);
		}
		
		outputStream.flush();
	}
	
}
